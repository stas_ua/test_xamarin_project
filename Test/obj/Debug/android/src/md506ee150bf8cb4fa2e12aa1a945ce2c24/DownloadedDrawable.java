package md506ee150bf8cb4fa2e12aa1a945ce2c24;


public class DownloadedDrawable
	extends android.graphics.drawable.ColorDrawable
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("Test.DownloadedDrawable, Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", DownloadedDrawable.class, __md_methods);
	}


	public DownloadedDrawable () throws java.lang.Throwable
	{
		super ();
		if (getClass () == DownloadedDrawable.class)
			mono.android.TypeManager.Activate ("Test.DownloadedDrawable, Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public DownloadedDrawable (int p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == DownloadedDrawable.class)
			mono.android.TypeManager.Activate ("Test.DownloadedDrawable, Test, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Graphics.Color, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
