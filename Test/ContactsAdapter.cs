﻿
using System;
using Android.Widget;
using Android.Graphics;
using System.Collections.Generic;
using Android.Content;
using Android.Views;

namespace Test
{
	public class Contact{
		public string Name{ get; set; }
		public string PhoneNum{ get; set; }
		public string Email{ get; set; }
		public Contact (String name, String phone, String email)
		{
			this.Name = name;
			this.PhoneNum = phone;
			this.Email = email;
		}
	};

	public class ContactsAdapter: BaseAdapter<Contact>
	{
		
		private List<Contact> _contactList;
		private Context _context;

		public ContactsAdapter (Context context, List<Contact> contactList)
		{
			_context = context;
			_contactList = contactList;
		}

		public override int Count {
			get { return _contactList.Count; }
		}

		public override	long GetItemId (int position)
		{
			return position;
		}

		public override Contact this [int position] {
			get { return _contactList [position]; }
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View row = convertView;
			if (row == null) {
				row = LayoutInflater.From (_context).Inflate (Resource.Layout.lwContactRow, null, false);
			}

			TextView textView = row.FindViewById<TextView> (Resource.Id.textView1);
	
			textView.Text =  _contactList [position].Name;

			return row;
		}


  
	}

}

