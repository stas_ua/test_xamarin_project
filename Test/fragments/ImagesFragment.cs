﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
//using Android.Util;
using Android.Views;
using Android.Widget;
//using Android.Graphics;
//using System.Net;
//using System.Threading.Tasks;
//using System.IO;

namespace Test
{
	public class ImagesFragment : Fragment
	{

		private Activity _context;
		public ImagesFragment(Activity context){
			_context = context;
		}


		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);

			base.OnCreateView (inflater, container, savedInstanceState);

			var view = inflater.Inflate (Resource.Layout.listOnTabFragment, container, false);

			ListView listView = view.FindViewById<ListView> (Resource.Id.listView1);
			listView.Adapter = getImageListAdapter();

			return view;
		}

		private ImageListAdapter getImageListAdapter(){

			//Android.Content.Context context = Android.App.Application.Context;

			var imgList = new List<string> ();
			for (int i = 100; i < 200; i+=2) {
				imgList.Add (("http://placekitten.com/200/" + i));
			}

			return new ImageListAdapter (_context, imgList);
		}






	}
}

