﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Provider;

namespace Test
{
	public class ContactsFragment : Fragment
	{
		private ContactsAdapter _adapter;
		private Activity _context;
		public ContactsFragment(Activity context ){			
			_context = context;

		}
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		

			// Create your fragment here
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			// Use this to return your custom view for this Fragment
			// return inflater.Inflate(Resource.Layout.YourFragment, container, false);

			base.OnCreateView (inflater, container, savedInstanceState);

			var view = inflater.Inflate (Resource.Layout.listOnTabFragment, container, false);			
			ListView listView = view.FindViewById<ListView> (Resource.Id.listView1);

			_adapter =  getCustomContactsAdapter();
			listView.Adapter = _adapter;

			RegisterForContextMenu(listView);
			listView.ItemClick += delegate(object sender, AdapterView.ItemClickEventArgs e) {				
				e.View.ShowContextMenu();
			};

			return view;
		}

		private ArrayAdapter<string> getContactsAdapter(){

			//var uri = ContactsContract.Contacts.ContentUri;
			var uri = ContactsContract.CommonDataKinds.Phone.ContentUri;
			string[] projection = { ContactsContract.Contacts.InterfaceConsts.Id,
				ContactsContract.Contacts.InterfaceConsts.DisplayName, ContactsContract.CommonDataKinds.Phone.Number };
			//var cursor = ManagedQuery (uri, projection, null, null, null);

			var loader = new CursorLoader (_context, uri, projection, null, null, null);
			var cursor = (Android.Database.ICursor)loader.LoadInBackground();
			var contactList = new List<string> ();

			if (cursor.MoveToFirst ()) {
				do {
					contactList.Add (cursor.GetString (
						cursor.GetColumnIndex (projection [1])));
				}  while (cursor.MoveToNext());
			}

			return new ArrayAdapter<string> (_context,
				Android.Resource.Layout.SimpleListItem1, contactList);
		}

		private ContactsAdapter getCustomContactsAdapter(){

			var uri = ContactsContract.Contacts.ContentUri;
			//var uri = ContactsContract.CommonDataKinds.Phone.ContentUri;
			string[] projection = { ContactsContract.Contacts.InterfaceConsts.Id,
				ContactsContract.Contacts.InterfaceConsts.DisplayName, //ContactsContract.CommonDataKinds.Phone.Number ,
				//ContactsContract.CommonDataKinds.Email.Address
			};
			//var cursor = ManagedQuery (uri, projection, null, null, null);

			var loader = new CursorLoader (_context, uri, projection, null, null, null);
			var cursor = (Android.Database.ICursor)loader.LoadInBackground();
			var contactList = new List<Contact> ();
			Android.Database.ICursor nestedCursor;
			string id;
			string phone = "";
			StringBuilder sb = new StringBuilder();

			if (cursor.MoveToFirst ()) {
				do {
					id = cursor.GetString (cursor.GetColumnIndex (projection [0])); 
					//id = cursor.GetString(cursor.GetColumnIndex(BaseColumns.Id));
					//****load email address
					loader = new CursorLoader (_context, ContactsContract.CommonDataKinds.Email.ContentUri, null,
						ContactsContract.CommonDataKinds.Email.InterfaceConsts.ContactId + " = " + id,
						//new String[]{ cursor.GetString(cursor.GetColumnIndex(BaseColumns.Id)) }
						null, null);
					nestedCursor = (Android.Database.ICursor)loader.LoadInBackground();

					if(nestedCursor.MoveToFirst()){
						do{
							sb.Append(nestedCursor.GetString(
								nestedCursor.GetColumnIndex(ContactsContract.CommonDataKinds.Email.InterfaceConsts.Data)))
								.Append(";");
						}while(nestedCursor.MoveToNext());
					}

					//****load phones
					loader = new CursorLoader (_context, ContactsContract.CommonDataKinds.Phone.ContentUri, null,
						ContactsContract.CommonDataKinds.Phone.InterfaceConsts.ContactId + " = " + id,
						//new String[]{ cursor.GetString(cursor.GetColumnIndex(BaseColumns.Id)) }
						null, null);
					nestedCursor = (Android.Database.ICursor)loader.LoadInBackground();

					if(nestedCursor.MoveToFirst()){						
						phone = nestedCursor.GetString(
								nestedCursor.GetColumnIndex(ContactsContract.CommonDataKinds.Email.InterfaceConsts.Data));						
					}

					//***
					contactList.Add (new Contact(
						cursor.GetString (cursor.GetColumnIndex (projection [1])),
						phone,
						sb.ToString()
					));
					sb.Clear();
					phone = "";
				}  while (cursor.MoveToNext());
			}

			return new ContactsAdapter (_context, contactList);
		}

		public override void OnCreateContextMenu(IContextMenu menu, View v, IContextMenuContextMenuInfo menuInfo)
		{
			if (v.Id == Resource.Id.listView1)
			{

				var info = (AdapterView.AdapterContextMenuInfo) menuInfo;
				String str = _adapter[info.Position].Name;
				menu.SetHeaderTitle (str); 
				var menuItems = Resources.GetStringArray(Resource.Array.menu);
				for (var i = 0; i < menuItems.Length; i++)
					menu.Add(Menu.None, i, i, String.Format("{0} {1}", menuItems[i], str));
			}
		}

		public override bool OnContextItemSelected(IMenuItem item)
		{
			var info = (AdapterView.AdapterContextMenuInfo) item.MenuInfo;
			var menuItemIndex = item.ItemId;
			var menuItems = Resources.GetStringArray(Resource.Array.menu);
			var menuItemName = menuItems[menuItemIndex];

			var listItemName = _adapter[info.Position].Name;

			switch (menuItemIndex) {
			case 0:
				if (_adapter [info.Position].PhoneNum.Length == 0) {
					Toast.MakeText(_context, string.Format("No phone number for a contact {0}!",  listItemName), ToastLength.Short).Show();
				} else {
					var uri = Android.Net.Uri.Parse ("tel:" + _adapter [info.Position].PhoneNum);
					var intent = new Intent (Intent.ActionDial, uri);
					StartActivity (intent);
				}
				break;
			case 1:
				if (_adapter [info.Position].Email.Length == 0) {
					Toast.MakeText(_context, string.Format("No email for a contact {0}!",  listItemName), ToastLength.Short).Show();
				} else {
					var email = new Intent (Android.Content.Intent.ActionSend);
					email.PutExtra (Android.Content.Intent.ExtraEmail,
						new string[]{ _adapter [info.Position].Email });

					email.PutExtra (Android.Content.Intent.ExtraSubject, String.Format ("Hello, {0}!", _adapter [info.Position].Name));

					email.PutExtra (Android.Content.Intent.ExtraText,
						String.Format ("Dear, {0} ...", _adapter [info.Position].Name));
				
					email.SetType ("message/rfc822");
					StartActivity (email);
				}

				break;			
			default:
				break;
			}
			//Toast.MakeText(_context, string.Format("Selected {0} for item {1}", menuItemName, listItemName), ToastLength.Short).Show();
			return true;
		}

		public void OnListItemClick(ListView l, View v, int position, long id){
			l.ShowContextMenuForChild(v);   
		}
	}
}

