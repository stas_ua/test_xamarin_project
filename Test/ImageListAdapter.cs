﻿using System;
using Android.Widget;
using Android.Graphics;
using System.Collections.Generic;
using Android.Content;
using Android.Views;

using System.Net;
using Android.Graphics.Drawables;

namespace Test
{
	public class ImageListAdapter: BaseAdapter<string>
	{
		private List<string> _imgList;
		private Context _context;

		public ImageListAdapter (Context context, List<string> imgList)
		{
			_context = context;
			_imgList = imgList;
		}

		public override int Count{
			get { return _imgList.Count;}
		}

		public override	long GetItemId(int position){
			return position;
		}

		public override string this[int position]{
			get { return _imgList [position]; }
		}

		public override View GetView(int position, View convertView, ViewGroup parent){
			View row = convertView;
			if (row == null) {
				row = LayoutInflater.From (_context).Inflate (Resource.Layout.lwImageRow, null, false);
			}

			ImageView imgView = row.FindViewById<ImageView> (Resource.Id.imageView1);
			//can't use because i have a limited version Xamarin, and project should be small
//			Square.Picasso.Picasso.With(_context)
//				.Load(_imgList[position])
//				.Into(imgView);
			//imgView.SetImageBitmap (GetImageBitmapFromUrl(_imgList[position]));
			var task = new BitmapDownloaderTask(imgView);
			imgView.SetImageDrawable(new DownloadedDrawable(task, Color.Gray));
			//imgView.SetImageDrawable(new ColorDrawable(Color.Gray));
			imgView.SetMinimumHeight(300);
			task.Execute(_imgList[position]);

			return row;
		}

		private Bitmap GetImageBitmapFromUrl(string url)
		{
			Bitmap imageBitmap = null;

			using (var webClient = new WebClient())
			{
				var imageBytes = webClient.DownloadData(url);
				if (imageBytes != null && imageBytes.Length > 0)
				{
					imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
				}
			}

			return imageBitmap;
		}
	}
}

