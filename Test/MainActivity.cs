﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Provider;
using System.Collections.Generic;

namespace Test
{
	[Activity (Label = "Test", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
		
			ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;
			SetContentView(Resource.Layout.Main);


			ActionBar.Tab tab = ActionBar.NewTab();
			tab.SetText(Resources.GetString(Resource.String.tab1_text));
			//tab.SetIcon(Resource.Drawable.tab1_icon);
			tab.TabSelected += delegate(Object sender,  ActionBar.TabEventArgs args)  {				
				args.FragmentTransaction.Replace(Resource.Id.frmCntr, new ContactsFragment(this));
			};
				ActionBar.AddTab(tab);


			tab = ActionBar.NewTab();
			tab.SetText(Resources.GetString(Resource.String.tab2_text));
			tab.TabSelected += delegate(Object sender,  ActionBar.TabEventArgs args)  {		
				args.FragmentTransaction.Replace(Resource.Id.frmCntr, new ImagesFragment(this));
			};
				ActionBar.AddTab(tab);

		}


	}
}


