﻿using System;
using Android.OS;
using Android.Graphics;
using Android.Widget;
using System.Net;
using System.IO;

namespace Test
{
	 //Stanislav Poberezhnyi: code from http://www.mobtowers.com/xamarin-android-using-remote-images-in-lists/

	public class BitmapDownloaderTask : AsyncTask<String, Java.Lang.Void, Bitmap>
	{
		//private readonly ImageView _imageView;
		private readonly WeakReference<ImageView> _imageViewReference;

		public BitmapDownloaderTask(ImageView imageView)
		{
			if (imageView == null)
				throw new ArgumentNullException("imageView");

			//_imageView = imageView;
			_imageViewReference = new WeakReference<ImageView>(imageView);
		}

		public string Url { get; private set; }

		/// <summary>
		/// Called on a background thread when the task is executed.
		/// </summary>
		protected override Bitmap RunInBackground(params string[] @params)
		{
			Url = @params[0];
			return DownloadRemoteImage(Url);
		}

		/// <summary>
		/// Once the image is downloaded, associates it to the imageView
		/// </summary>
		protected override void OnPostExecute(Bitmap bitmap)
		{
			if (IsCancelled)
				bitmap = null;

			if (_imageViewReference != null && bitmap != null)
			{
				ImageView imageView;
				if (!_imageViewReference.TryGetTarget(out imageView))
					return;

				var bitmapDownloaderTask = imageView.GetBitmapDownloaderTask();

				// Change bitmap only if this process is still associated with it.
				// This is necessary as views can be reused by Android, and a newer BitmapDownloader instance may have been attached to it.
				if (this == bitmapDownloaderTask)
					imageView.SetImageBitmap(bitmap);

			}
		}

		private Bitmap DownloadRemoteImage(string url)
		{
			if (string.IsNullOrWhiteSpace(url))
				throw new ArgumentException("url must not be null, empty, or whitespace");

			Uri imageUri;
			if (!Uri.TryCreate(url, UriKind.Absolute, out imageUri))
				throw new ArgumentException("Invalid url");

			try
			{
				WebRequest request = HttpWebRequest.Create(imageUri);
				request.Timeout = 10000;

				WebResponse response = request.GetResponse();
				Stream inputStream = response.GetResponseStream();

				return BitmapFactory.DecodeStream(inputStream);
			}
			catch (Exception)
			{
				return null;
			}
		}
	}
}

