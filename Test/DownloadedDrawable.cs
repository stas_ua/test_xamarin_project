﻿using System;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Widget;

namespace Test
{

	//Stanislav Poberezhnyi: code from http://www.mobtowers.com/xamarin-android-using-remote-images-in-lists/

	/// <summary>
	/// A fake Drawable that will be attached to the ImageView while the download is in progress.
	/// Contains a reference to the actual download task, so that a download task can be stopped
	///  if a new binding is required, and makes sure that only the last started download process can
	///  bind its result, independently of the download finish order.
	/// </summary>
	public class DownloadedDrawable : ColorDrawable
	{
		private readonly WeakReference<BitmapDownloaderTask> _bitmapDownloaderTaskReference;

		public DownloadedDrawable(BitmapDownloaderTask bitmapDownloaderTask, Color loadingBackgroundColor)
			: base(loadingBackgroundColor)
		{
			_bitmapDownloaderTaskReference = new WeakReference<BitmapDownloaderTask>(bitmapDownloaderTask);
		}

		public BitmapDownloaderTask GetBitmapDownloaderTask()
		{
			BitmapDownloaderTask task;
			_bitmapDownloaderTaskReference.TryGetTarget(out task);
			return task;
		}

	}

	public static class DrawHerlper{


		/// <summary>
		/// Retrieve the BitmapDownloaderTask from the ImageView's drawable.
		/// This will return null if the drawable is not a DownloadedDrawable, or there is no BitmapDownloaderTask attached to it.
		/// </summary>
		public static BitmapDownloaderTask GetBitmapDownloaderTask(this ImageView imageView)
		{
			if (imageView != null)
			{
				var drawable = imageView.Drawable as DownloadedDrawable;
				if (drawable != null)
					return drawable.GetBitmapDownloaderTask();
			}

			return null;
		}
	}
}

